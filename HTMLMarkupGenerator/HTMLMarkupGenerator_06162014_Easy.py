__author__ = 'Joseph'
import os
import webbrowser

with open("output.html", "w") as output:
    paragraph = raw_input("Enter your paragraph: \n")
    output.write("""<!DOCTYPE html>
    <html>
        <head>
            <title></title>
        </head>

        <body>
            <p>%s</p>
        </body>
    </html>""" % paragraph)

# open the web browser to display the output
webbrowser.open("file://%s" % os.path.abspath("output.html"))
