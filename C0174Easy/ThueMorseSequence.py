__author__ = 'Joseph'


def header():
    """Print the header of the table"""
    print "nth\t\tSequence"
    print "==========================================================================="


def update_sequence(old_sequence):
    """Make the second half of the new sequence by inverting and appending the old sequence"""
    new_sequence = ""
    for i in range(0, len(old_sequence)):
        if old_sequence[i] == "0":
            new_sequence += "1"
        else:
            new_sequence += "0"
    return old_sequence + new_sequence


def print_sequence(n):
    """Print a Thue Morse sequence of length n to the console"""
    current_sequence = "0"
    for i in range(n):
        print i, "\t\t", current_sequence
        current_sequence = update_sequence(current_sequence)

if __name__ == "__main__":
    print_sequence(10)