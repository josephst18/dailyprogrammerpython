
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'My Project',
    'author': 'Joseph Stahl',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'joseph@josephstahl.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['C0163Easy'],
    'scripts': [],
    'name': 'C0163Easy'
}

setup(**config)
