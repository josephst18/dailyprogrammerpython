__author__ = 'Joseph'

import random

random.seed()

def count(power_10, face_sides):
    """
    Count how many times each face of the die is rolled.  Counts will carry over between rolls (for example, the counts
    from the first 10 rolls will be carried over to the counts from the next 90 rolls to populate the 100 rolls data).

    Can use different powers of 10 but will always use a six-sided die (in current configuration).

    :param power_10: power of 10 to determine max number of results to be reported
    (i.e. power_10 = 6 --> 1000000 results)
    :param face_sides: number of faces on the die (a standard die has 6 faces)
    :return: list of lists, length determined by power_10 with each sublist containing six integer counts of
    roll frequencies
    """
    tracklist = [[0 for x in range(face_sides)] for x in range(power_10)]
    _upperlimit = 1  # start upper limit at 1 so multiplying it by 10 does not yield 0
    _maximum = 10 ** power_10
    _rollcount = 0
    _sublist = 0  # begin filling in results in the first sublist of tracklist

    # begin simulating rolls, stopping at every power of 10 to record & rollover results
    while _upperlimit < _maximum:
        _upperlimit *= 10  # start at 10 rolls, then 100, then 1000, etc.
        while _rollcount < _upperlimit:
            # track how many times each roll occurred and increment # of rolls
            tracklist[_sublist][roll(face_sides)] += 1
            _rollcount += 1
        # rollover old list to new list IF NOT AT LAST POSITION
        if _sublist + 1 < len(tracklist):
            for x in range(face_sides):
                tracklist[_sublist + 1][x] += tracklist[_sublist][x]
        _sublist += 1

    return tracklist

def roll(face_sides):
    """
    Roll a die with number of faces equal to face_sides.
    :param: face_sides: face_sides: number of faces on the die (a standard die has 6 faces)
    :return: integer result from 0 to face_sides (corresponding to sides 1-(max) on the die)
    """
    return random.randrange(0,face_sides)

def percentify(results):
    """
    Convert absolute counts into relative counts (convert to percentages).  Counts up total rolls at each power of 10
    and divides each roll by this number (floating point because results will be < 1.00) and multiplies by 100 to
    form a percentage.
    :param results: absolute count of roll results, formatted as a list of lists
    :return: relative counts of roll results, still formatted as a list of lists
    """
    percentages = [[0 for x in range(len(results[0]))] for x in range(len(results))]
    for sublist in range(len(percentages)):
        total_rolled = 10 ** (1 + sublist)
        for face in range(len(results[0])):  # len(results[0]) = number of die faces
            # NOTE:  this results in full length floating point answer that needs formatting for pretty display
            # make sure to use floating point division
            percentages[sublist][face] = (results[sublist][face] / float(total_rolled) * 100.0)
    return percentages

def makepretty(percentlist):
    """
    Make the percentage list of lists into a human readable format by truncating to 2 decimal places and flatteing
    the list of lists into a single list of strings.  Each string holds the results of one power-of-10 set of rolls with
    each percentage separated by a tab.
    :param percentlist: percentage list of lists; percentlist[sublist] holds the lists for the results of each
    power-of-10 roll
    :return: human readable list suitable for printing by string formatting
    """
    stringlist = ["" for x in range(len(percentlist))]
    # make tab delimited string
    for sublist in range(len(percentlist)):
        for value in range(len(percentlist[sublist])):
            stringlist[sublist] += ("%04.2f%%\t" % percentlist[sublist][value])
    return stringlist

def print_results(percentlist):
    """
    Print results in a nicely-formatted table.  Height of table is determined by power of 10 chosen (i.e. 10^6 prints
    six rows of results).  Number of different faces is determined by length of percentlist.
    :param percentlist: list of roll percentages determined by percentify()
    :param power: the power of 10 selected at the beginning of simulation to set number of rolls to be simulated
    :return: formatted table (tab delimited) listing the percentages of each face rolled.
    """
    # Convert list to readable format
    prettylist = makepretty(percentlist)
    # Make a string for different faces of the die, separated by 2 tabs each.  Number of faces is equal to length
    # of percentlist.  String replacement and list comprehension allow this list to be as long as necessary.
    different_faces = "\t\t".join((("%ss" % (x + 1)) for x in range(len(percentlist[0]))))
    # NOTE:  all spacing here is 2 tabs (16 spaces).  Total 16 spaces to beginning of percents.
    print "# of Rolls \t\t" + different_faces
    print "=================" + "========" * len(percentlist[0])
    for position in range(len(percentlist)):
        # left align the 10^power and print string from powerlist[position]
        # formatting prettylist[position] as a string allows the tab characters added in by makepretty() to take effect
        print "%-16d%s" % (10**(position + 1), prettylist[position])


if __name__ == "__main__":
    # WARNING:  if power_10 is > 7, program will take a very long time to run.  If > 9, program will fail.
    # suggested range for power_10 is 1 - 7
    power_10 = 6
    face_sides = 6
    print_results(percentify(count(power_10, face_sides)))