__author__ = 'Joseph'


def get_input():
    return raw_input("Enter string: ").upper()


def process(to_translate):
    """
    Process a string into a PBM-formatted string suitable for writing
    :param to_translate: string for making into a PBM image
    :return:    PBM image string (typically used for writing)
    """
    fileBuffer = "P1\n"  # P1 format for PBM
    fileBuffer += "%i %i\n" % (len(to_translate)*5, 7) # image is length*5 pixels wide, 7 tall
    _bitmap_array = []
    for character in to_translate: # add characters to array during translation
        _bitmap_array.append(translate(character))
    # when translation is complete, write out the array horizontally in file
    for i in range(7):
        row = ""
        for j in range(len(to_translate)):
            row += _bitmap_array[j][i] + " " # take line 1, 8, 15, etc. to end of array.  Then 2, 9, 16, etc. to end...
        fileBuffer += row + "\n"
    return fileBuffer


def translate(character):
    """
    Translate a given character into its PBM equivalent.  Translation is done by converting a list of characters and
    their bitmap representation (thanks  /u/chunes) into a dictionary, with keys of characters ('A', 'B', etc.)
    and values of bitmap representations, with each bitmap representation being a list (length 7) of lists (length 5).
    So each bitmap is a 5x7 grid of 0 & 1 (space separated) where 1 corresponds to a black pixel.
    :param character:   character to be translated into bitmap
    :return:    list of lists representing the bitmap of the given character
    """
    alphabet = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ ") # space (" ") is in position 26
    with open("font.txt", 'r') as font:
        input_array = font.readlines()
        output_array = []
        for i in range(len(alphabet)):
            single_char = []
            for j in range(7):  # skip first character (the character) and go to binary representation
                single_char.append((input_array[j + (8*i + 1)]).strip())  # go to correct position in array, skip first characters
            output_array.append(single_char)
        # make dictionary of alphabet keys with bitmap (5x7 grid/ list of lists) values
        font_dict = {alphabet[i]: output_array[i] for i in range(len(alphabet))}

    return font_dict.get(character)

if __name__ == "__main__":
    input_text = get_input()
    bitmap = process(input_text)
    with open("output.pbm", "w") as output_file:
        output_file.write(bitmap)