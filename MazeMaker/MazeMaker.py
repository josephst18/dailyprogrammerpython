from __future__ import print_function
__author__ = 'Joseph'


import os, sys, getopt
from PIL import Image


def imagetoarray(im):
    """
    Convert a black and white bitmap image into an array.
    White pixels have value 0; black pixels have value 1.  The image is read into a multidimensional
    array where len(a[]) is equal to the height of the image and len(a[0]) is equal to the width of the image.
    :param im: image to store as array
    :return:    array representation of black and white bitmap image
    """
    width, height = im.size

    a = [[] for y in range(height)] # make 2-dimensional array to hold pixel values for each row

    # list is now proper length
    for y in range(height):
        for x in range(width):
            a[y].append(im.getpixel((x, y)))
    return a


def writeOutput(a, outfile):
    """
    Write output from array to a file.
    A header (width height) is written to the output file and then the array is printed to the file.
    The array is a multidimensional array where len(a[]) is equal to the height of the image
    and len(a[0]) is equal to the width of the image.

    :param a:   2 dimensional array containing values of 0 (black) and 1 (white)
    :param outfile: output text file to store maze in
    :return:    writes .txt file to same directory as input bitmap image
    """
    outfile.write('%i %i \n' % (len(a), len(a[0])))  # write maze dimensions
    for y in a:
        output = ""
        for x in y:
            if (x == 0):
                output += ("#")
            else:
                output += (" ")
        outfile.write("%s\n" % output) # use windows line endings
    print("Done writing output")


def usage():
    print ("Bitmap to maze converter by Joseph Stahl")
    print ("Usage:")
    print ("-i || --infile=\t\tInput file")
    print ("-o || --outfile=\t\tOutput file")
    print ("-h || --help\t\tPrint this help")


def main(argv):
    """
    Convert a bitmap image (representing a maze) into a text file.  Black pixels are walls of the maze and
    will be represented by pound signs ("#").  White pixels are paths that can be traveled and will be left blank
      in the output file.

    If using this program to create a maze, it is the job of the user to specify start and end points after
    the text file has been created.
    :param argv: -i is input filename, -o is output filename
    :return:
    """
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["help", "infile=", "outfile"])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-i", "--infile"):
            infile = arg
        elif opt in ("-o", "--outfile"):
            outname = arg

    f, e = os.path.splitext(infile)
    if e == ".bmp":
        print("Converting bitmap to text file")
        outfile = open(outname, 'w')
        try:
            im = Image.open(infile)
            a = imagetoarray(im)
            # NOTE:  black pixels are given value of 0; white pixels are 1.
            writeOutput(a, outfile)
            print ("Success.  Output written to %s" % outname)
        except IOError:
            print("Cannot read for conversion ", infile)
        outfile.close()
    else:
        print("File does not have bitmap extension")

if __name__ == "__main__":
    if (len(sys.argv[1:]) == 0):
        usage()
        sys.exit(2)
    main(sys.argv[1:])