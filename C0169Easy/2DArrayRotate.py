__author__ = 'Joseph'


class Array(object):
    def __init__(self, size):
        """
        Create 2D array, a (size)x(size) dimension grid.  Populated with 0s by default
        :param size: size of array to create, both length and width
        :return:
        """
        self.size = size
        self.a = [[0 for i in range(self.size)] for i in range(self.size)]

    def premade_array(self, sublists):
        """
        Make a 2D array from a provided list of sublists (after checking for correct form).  This is useful for
        repeated testing with the same input.
        :param sublists:    list of sublists to be turned into a 2D array
        :return:
        """
        # make sure input is acceptable
        assert isinstance(sublists, list) and isinstance(sublists[0], list) # is a list containing sublists
        assert len(sublists) == self.size and len(sublists[0]) == self.size # is a box of correct size
        assert isinstance(sublists[0][0], int) # is a list of sublists of integers

        for i in range(self.size):
            self.a[i] = sublists[i]

    def print_array(self):
        """
        Print an array to the console
        :return:
        """
        for i in range(self.size):
            current = ""
            for j in range(self.size):
                current += str(self.a[i][j]) + " "
            print(current)

    def array_to_string(self):
        """
        Convert an array to a string.
        Not very useful right now, but I suppose it could be useful if this class is imported.
        :return: string representation of the 2D array.  Ints are converted to strings with a space separating each int
        """
        output = ""
        for i in range(self.size):
            current = ""
            for j in range(self.size):
                current += str(self.a[i][j]) + " "
            output += (current + "\n")
        return output

    def populate_array(self):
        """
        Populate a 2D array with user input.  Input is put in over multiple lines and separated by a space character.
        An empty line will also stop input.
        :return:
        """

        print("Enter lines of input, separated by spaces.  Empty line to stop: ")

        temp_array = []

        # multiline input from https://stackoverflow.com/questions/11664443/raw-input-across-multiple-lines-in-python
        # read multiline user input to create list
        sentinel = '' # end at sentinel character
        for line in iter(raw_input, sentinel):
            temp_array.append(line)

        # make sure user entered enough input
        assert isinstance(temp_array, list)
        assert len(temp_array) == self.size

        # convert strings from input into a list of lists (2D array)
        try:
            for i in range(self.size):
                split_list = str(temp_array[i]).split()
                for j in range(len(split_list)):
                    self.a[i][j] = split_list[j]
        except TypeError:
            print("Array could not be created. (TypeError)")
        except IndexError:
            print("IndexError occurred")

    def rotate(self):
        """
        Rotate a 2D array 90 clockwise
        :return:
        """
        a_rotate = [] # no need to populate array when appending to it
        for i in range(self.size):
            original_column = []
            for j in range(self.size):
                # take first int off each row, then second int off each row, etc.
                original_column.append(self.a[j][i])
            # for clockwise rotation, column must be reversed (use list slicing)
            a_rotate.append(original_column[::-1])

        # store the rotated array in place of the original so that more rotations can take place later
        self.a = a_rotate



if __name__ == "__main__":
    # premade array for testing
    small_array = Array(3)
    small_array.premade_array([[0, 0, 0], [1, 1, 1], [2, 2, 2]])
    print small_array.array_to_string()
    small_array.rotate()
    print "clockwise rotation: "
    small_array.print_array()

    # array of any size
    manual_array = Array(int(raw_input("enter size: ")))
    manual_array.populate_array()
    manual_array.print_array()
    manual_array.rotate()
    print "clockwise rotation: "
    manual_array.print_array()

